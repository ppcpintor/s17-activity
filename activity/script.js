let enrolees = ["Bubbles", "Buttercup", "Blossom", "Professor"];
let students = ["Baldo", "Tots", "Tyang"];


function addStudent(){
    students.push(prompt("Type the Student's Name: "));
}

function printStudents(){
    students.sort();
    students.forEach(function(student){
        console.log(student);
    });
}

function countStudents(){
    console.log("The number of students: " + students.length);
}

function studentSearch(compare){
    let multipleEnroll = [];
    let studentCounted = 0;
    students.filter(function find(student) {
        if(student.toLowerCase() === compare.toLowerCase()){
            studentCounted++;
            multipleEnroll.push(student);
        }

    });

    if(studentCounted > 1){
        console.log(`${multipleEnroll} are students / enrollees`);
    }
    else if(studentCounted === 1){
        console.log(`${multipleEnroll} is a student / an enrollee`)
    }
    else console.log(`${compare} is not a student / an enrollee`);
};
